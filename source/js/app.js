$("header").on("show.bs.collapse", function () {
  $("header").addClass("bg-detalhe");
  $("header").removeClass("bg-yellow");
});
$("header").on("hide.bs.collapse", function () {
  $("header").addClass("bg-yellow");
  $("header").removeClass("bg-detalhe");
});
var carouselMaquinas = tns({
  container: ".maquinas",
  mode: "carousel",
  items: 1,
  slideBy: 1,
  autoplay: false,
  gutter: 85,
  edgePadding: 85,
  loop: false,
  nav: false,
  touch: false,
  controlsText: [
    '<i class="fas fa-chevron-left"></i>',
    '<i class="fas fa-chevron-right"></i>',
  ],
  responsive: {
    768: {
      items: 2,
    },
    1024: {
      items: 4,
      gutter: 40,
    },
  },
});
var carouselSolucoes = tns({
  container: ".carosel-solucoes",
  mode: "carousel",
  items: 1,
  slideBy: 1,
  autoplay: false,
  gutter: 0,
  edgePadding: 0,
  loop: false,
  nav: false,
  touch: false,
  controlsText: [
    '<i class="fas fa-chevron-left"></i>',
    '<i class="fas fa-chevron-right"></i>',
  ],
  responsive: {
    768: {
      items: 2,
    },
    1024: {
      items: 6,
      gutter: 0,
      edgePadding: 0,
    },
  },
});

$("a.btn-geral").on("click", function (e) {
  e.preventDefault();
  $("#navbarSupportedContent").removeClass("show");
  let foco = $(".lead-mobile")[0].offsetTop;
  window.scrollTo(0, foco);
});
$('.btn-aceito').on('click',function(e){
  e.preventDefault();
  $('.politica').style.opacity="0"
})
$(window).on("scroll", function () {
  if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
    $(".etiqueta").addClass("d-none");
  } else {
    $(".etiqueta").removeClass("d-none");
  }
});

$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$("input[name=email-desktop]", "input[name=email-mobile]").attr(
  "onblur",
  "validateEmail(this)"
);

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

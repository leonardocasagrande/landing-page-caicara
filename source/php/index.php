<?php get_header(); ?>
<section class="carosel">
    <div class="maquinas">
        <div class="item">
            <img src="<?= the_field('img_min_auto', 61)?>" alt="Maquina de Café">
        </div>
        <div class="item">
            <img src="<?= the_field('img_min_semi_multi', 61)?>" alt="Maquina de Café">
        </div>
        <div class="item">
            <img src="<?= the_field('img_min_auto_multi', 61)?>" alt="Maquina de Café">

        </div>
        <div class="item">
            <img src="<?= the_field('img_min_profissional', 61)?>" alt="Maquina de Café">

        </div>
    </div>

</section>
<section class="produtos">
    <div class="cabecalho text-center">
        <div class="row">
            <div class="col-lg-6">
                <h2 class="color-yellow text-center">Alugue a máquina perfeita para a necessidade do seu negócio!</h2>
                <i>Conheça cada modelo para locação:</i>
            </div>
            <div class="col-lg-6 text-lg-left">
                <h3 class="color-red">Cidades de Atuação:</h3>
                <div class="row">
                    <?php the_field('mobile', 32) ?>
                    
                    <?php the_field('desktop', 32) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="linha-produto esquerda">
        <div class="container">
            <div class="row  align-items-center">
            <div class="col-lg-4  foto-maquina-automatica">
            <img src="<?= the_field('imagem_auto', 61)?>" alt="">
        </div>
        <div class="col-lg-8 maquina-automatica maq-auto pr-lg-4">
            <h2 class="color-red"><?= the_field('titulo_auto', 61)?></h2>
            <div class="detalhe bg-red"></div>
            <?= the_field('conteudo_auto', 61)?>
            <a href="#orcamento" class="btn-geral bg-red color-white"><i>PEÇA SEU ORÇAMENTO</i></a>
        </div>
            </div>
        </div>
    </div>
    <div class="row no-gutters align-items-center linha-produto p-77 flex-wrap-reverse justify-content-end">

        <div class="col-lg-5 maquina-automatica maq-semi pl-lg-4">
            <h2 class="color-red"><?= the_field('titulo_semi_multi', 61)?></h2>
            <div class="detalhe bg-red"></div>
            <?= the_field('conteudo_semi_auto', 61)?>
            <a href="#orcamento" class="btn-geral btn-geral-2 bg-yellow color-red"><i>PEÇA SEU ORÇAMENTO</i></a>
        </div>
        <div class="col-lg-6  foto-maquina-semiautomatica">
            <img src="<?= the_field('imagem_semi_multi', 61)?>" alt="">
            <div class="selo bg-red"><i>Opção de<br>modelos com<br>e sem chá</i></div>
        </div>
    </div>
    <div class="row no-gutters align-items-center linha-produto direita">
        <div class="col-lg-6  foto-maquina-muitl">
            <img src="<?= the_field('imagem_auto_multi', 61)?>" alt="">
            <div class="selo bg-red"><i>Comporta<br>sistema de<br>cobrança</i></div>
        </div>
        <div class="col-lg-5 maquina-automatica maq-multi pr-lg-4">
            <h2 class="color-red"><?= the_field('titulo_auto_multi', 61)?></h2>
            <div class="detalhe bg-red"></div>
            <?= the_field('conteudo_auto_multi', 61)?>
            <a href="#orcamento" class="btn-geral btn-geral-2 bg-red color-white"><i>PEÇA SEU ORÇAMENTO</i></a>
        </div>

    </div>
    <div class="row no-gutters align-items-center linha-produto flex-wrap-reverse justify-content-end">

        <div class="col-lg-5 maquina-automatica maq-prof pl-lg-4">
            <h2 class="color-red"><?= the_field('titulo_profissional', 61)?></h2>
            <div class="detalhe bg-red"></div>
            <?= the_field('conteudo_profissional', 61)?>
            <a href="#orcamento" class="btn-geral btn-geral-2 bg-yellow color-red"><i>PEÇA SEU ORÇAMENTO</i></a>
        </div>
        <div class="col-lg-6  foto-maquina-profissional">
            <img src="<?= the_field('imagem_profissional', 61)?>" alt="">
        </div>
    </div>
    </div>
</section>
<section class="solucoes bg-grey">
    <div class="container">
        <div class="cabecalho text-center">
            <i>Cada lugar tem<br class="d-lg-none"> &nbsp;uma necessidade</i>
            <h2 class="color-red">NÓS TEMOS TODAS AS<br class="d-lg-none"> &nbsp;SOLUÇÕES!</h2>
        </div>
    </div>
    <div class="carosel-solucoes">
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/empresas.png" alt="">
            </div>
            <div class="texto">
                <i>Empresas de <br> todos os portes</i>
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/escritorios.png" alt="">
            </div>
            <div class="texto">
                <i>Escritórios</i>
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/hospitais.png" alt="">
            </div>
            <div class="texto">
                <i>Hospitais e<br> consultorios</i>
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/restaurantes.png" alt="">
            </div>
            <div class="texto">
                <i>Restaurantes</i>
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/escolas.png" alt="">
            </div>
            <div class="texto">
                <i>Escolas e <br> Faculdades</i>
            </div>
        </div>
        <div class="item">
            <div class="img">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cafeteria.png" alt="">
            </div>
            <div class="texto">
                <i>Cafeterias</i>
            </div>
        </div>

    </div>
</section>
<?php get_footer(); ?>